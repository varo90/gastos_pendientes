<?php
    include('functions.php');
    
    $date = $_POST['date'];
    $pedido = get_pedidos($date);
    $cabecera_tallas = array();
    $tallas = array();
    $tallas_use = array();
    $mounted = array();
    $modelo_ant = '';
    $tejido_ant = '';
    $ref_color_ant = '';
    foreach($pedido as $cont => $ped) {
        $modelo = $ped['MODELO'];
        $tejido = $ped['TEJIDO'];
        $color_d = explode('\\',$ped['COLOR']);
        $ref_color = $color_d[0];
        $color = $color_d[1];
        $precio = $ped['PRECIO'];
        $talla = end(explode(' ',$ped['DESCRIPCION']));
        $comentario = utf8_encode($ped['COMENTARIO']);
        if((($modelo != $modelo_ant) || ($tejido != $tejido_ant) || ($ref_color != $ref_color_ant)) && ($cont > 0)) {
            $mounted[] = array('MODELO'=>$modelo_ant, 'TEJIDO'=>$tejido_ant, 'REF_COLOR'=>$ref_color_ant, 'COLOR'=>$color_ant, 'PRECIO'=>$precio_ant, 'TALLAS'=>$tallas, 'COMENTARIO'=>$comentario_ant);
            $tallas = array();
        }
        $tallas[$talla] = $ped['CANTIDAD'];
        $modelo_ant = $modelo;
        $tejido_ant = $tejido;
        $ref_color_ant = $ref_color;
        $color_ant = $color;
        $precio_ant = $precio;
        $comentario_ant = $comentario;
        $cabecera_tallas[] = $talla;
    }
    $mounted[] = array('MODELO'=>$modelo_ant, 'TEJIDO'=>$tejido_ant, 'REF_COLOR'=>$ref_color_ant, 'COLOR'=>$color_ant, 'PRECIO'=>$precio_ant, 'TALLAS'=>$tallas, 'COMENTARIO'=>$comentario_ant);
    $cabecera_tallas = array_unique($cabecera_tallas);
    asort($cabecera_tallas);
?>
    <table id="dataTable" class="table table-striped">
      <tr>
        <th scope="col">MODELO</th>
        <th scope="col">TEJIDO</th> 
        <th scope="col">REF.COLOR</th>
        <th scope="col">COLOR</th>
        <th scope="col">PRECIO</th>
        <?php 
        foreach($cabecera_tallas as $talla) {
        ?>
            <th scope="col"><?php echo $talla; ?></th>
        <?php
        }
        //reset($cabecera_tallas);
        ?>
        <th scope="col">TOTAL</th>
        <th scope="col">TOTAL €</th>
        <th scope="col">Comentario</th>
      </tr>
<?php
    foreach($mounted as $cont => $ped) {
        $modelo = $ped['MODELO'];
        $tejido = $ped['TEJIDO'];
        $ref_color = $ped['REF_COLOR'];
        $precio = $ped['PRECIO'];
        $color = $ped['COLOR'];
        $comentario = $ped['COMENTARIO'];
        $tallas = $ped['TALLAS'];
        $cantidad = array_sum($tallas);
        $total = $cantidad*$precio;
        echo '<tr>';
        echo "<td>$modelo</td>";
        echo "<td>$tejido</td>";
        echo "<td>$ref_color</td>";
        echo "<td>$color</td>";
        echo "<td>$precio</td>";
        $num_col = 0;
        foreach($cabecera_tallas as $col_name) {
            if(array_key_exists($col_name, $tallas)) {
                echo "<td>$tallas[$col_name]</td>";
            } else {
                echo "<td></td>";
            }
        }
        echo "<td>$cantidad</td>";
        echo "<td>$total</td>";
        echo "<td>$comentario</td>";
        echo '</tr>';
    }
?>
</table>
<button id="get_file" class="btn btn-success" type="button">Obtener CSV</button>
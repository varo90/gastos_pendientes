<?php

include('db_connections.php');
include('/var/www/UTILS/PHPMailer/src/SMTP.php');
include('/var/www/UTILS/PHPMailer/src/PHPMailer.php');

function get_cobros($date_until) {
    $albaranes = get_albaranes($date_until);
    $pedidos = get_pedidos($date_until);
    $devoluciones = get_devoluciones($date_until);
    
    $cobros = array_merge($albaranes,$pedidos,$devoluciones);
    asort($cobros);
    return $cobros;
}

function get_proveedores($cobros) {
    $proveedores = array();
    foreach($cobros as $cobro) {
        $proveedores[$cobro->CardCode] = $cobro->CardName;
    }
    $provs_unique = array_unique($proveedores);
    return $provs_unique;
}

function get_rows($proveedores,$cobros) {
    $rows = array();
    $prov_id_ant = '';
    foreach($proveedores as $prov_id => $prov_name) {
        if($prov_id_ant != $prov_id) {
            $rows[] = get_totales($prov_id,utf8_encode($prov_name),$cobros,$prov_id_ant);
        }
    }
    return $rows;
}

function get_totales($prov_id,$prov,$cobros,&$prov_id_ant) {
    $cols = array('Proveedor'=>'','albaran'=>'','pedido'=>'','factura'=>'');
    $cols['Proveedor'] = $prov;
    foreach($cobros as $cobro) {
        if($cobro->CardCode == $prov_id) {
            $cols[$cobro->Tipo] = number_format($cobro->Total,2,',','');
        }
        $prov_id_ant = $prov_id;
    }
    return $cols;
}
//http://intranet/gastomes/generate_file.php
// ALBARANES
function get_albaranes($date_until) {
    $sql = "SELECT 'albaran' AS Tipo, op.CardCode, op.CardName COLLATE Modern_Spanish_CI_AS AS CardName, (SUM(pd.GTotal)-SUM(pd.GTotal*op.DiscPrcnt/100)) AS Total
FROM OPDN op WITH (NOLOCK)
        LEFT JOIN PDN1 pd WITH (NOLOCK) ON pd.DocEntry = op.DocEntry AND pd.LineStatus='O'
WHERE (op.DocStatus = 'O') AND (pd.DocDate <= '$date_until') AND (LEFT(op.CardCode, 1) = 'P')
GROUP BY op.CardCode, op.CardName
ORDER BY op.CardName";
    return make_query($sql);
}

// PEDIDOS
function get_pedidos($date_until) {
    $sql = "SELECT 'pedido' as Tipo, op.CardCode, op.CardName collate Modern_Spanish_CI_AS as CardName, (SUM(pd.GTotal)-SUM(pd.GTotal*op.DiscPrcnt/100)) AS Total
FROM OPOR op with (NOLOCK)
        LEFT JOIN POR1 pd with (NOLOCK) ON pd.DocEntry = op.DocEntry AND pd.LineStatus='O'
WHERE (op.DocStatus = 'O') AND (pd.DocDate <= '$date_until') AND (LEFT(op.CardCode, 1) = 'P')
GROUP BY op.CardCode, op.CardName
ORDER BY op.CardName";
    return make_query($sql);
}

// DEVOLUCIONES (BONOS)
function get_devoluciones($date_until) {
    $sql = "SELECT 'factura' as Tipo, op.CardCode, op.CardName collate Modern_Spanish_CI_AS as CardName, (SUM(pd.GTotal)-SUM(pd.GTotal*op.DiscPrcnt/100)) AS Total
FROM ORPD op with (NOLOCK)
        LEFT JOIN RPD1 pd with (NOLOCK) ON pd.DocEntry = op.DocEntry AND pd.LineStatus='O'
WHERE (op.DocStatus = 'O') AND (pd.DocDate <= '$date_until') AND (LEFT(op.CardCode, 1) = 'P')
GROUP BY op.CardCode, op.CardName
ORDER BY op.CardName";
    return make_query($sql);
}
    
function make_query($sql) {
    $ms_conn = mssql_connection_test();

    $query = $ms_conn->prepare($sql);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_OBJ);

    disconnect($ms_conn);

    return $result;
}

function send_email($route_file) {
    $to=array();
    $to[] = 'adriana@santaeulalia.com';
    $to[] = 'marco@santaeulalia.com';
    $mes = nombremes();
    $anio = date('Y');
    $subject = "Gastos pendientes $mes $anio";
    $message = 'Reporte de gastos pendientes por acreedor.';
    //$to = array('jbaladon@santaeulalia.com','asantos@santaeulalia.com');
    $cc = array('jbaladon@santaeulalia.com','asantos@santaeulalia.com');

    $mail = new PHPMailer(true);
    //Luego tenemos que iniciar la validación por SMTP:
    $mail->IsSMTP();
    $mail->SMTPAuth = true;
    $mail->Host = 'smtp.office365.com'; // Aquí pondremos el SMTP a utilizar. Por ej. mail.midominio.com
    $mail->Username = 'it@santaeulalia.com'; // Email de la cuenta de correo. ej.info@midominio.com La cuenta de correo debe ser creada previamente. 
    $mail->Password = 'Informatica2013'; // Aqui pondremos la contraseña de la cuenta de correo
    $mail->Port = 587; // Puerto de conexión al servidor de envio. 
    $mail->From = 'it@santaeulalia.com'; // Desde donde enviamos (Para mostrar). Puede ser el mismo que el email creado previamente.
    $mail->FromName = 'Solicitudes IT Santa Eulalia'; //Nombre a mostrar del remitente. 
    foreach($to as $mail_address) {
        $mail->AddAddress($mail_address); // Esta es la dirección a donde enviamos 
    }
    foreach($cc as $mail_address) {
        $mail->AddCC($mail_address);
    }
    $mail->IsHTML(true); // El correo se envía como HTML 
    $mail->Subject = utf8_decode($subject); // Este es el titulo del email. 
    $body = '<font style="font:15px Arial;">' . utf8_decode($message) . '<br>'; 
    $mail->Body = $body; // Mensaje a enviar. 
    $mail->addAttachment($route_file);
    try {
        $mail->Send(); // Envía el correo.
    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }
}

function nombremes(){
 $mes = date('n');
 setlocale(LC_TIME, 'spanish');  
 $nombre=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
 return $nombre;
} 

<?php
    include('session_init.php');
    if (((empty($_SESSION['username_link']) || !isset($_SESSION['username_link']))
            || ($_SESSION['usergroup_link'] != 1 && $_SESSION['userdpto_link'] != 7)) && basename($_SERVER['SCRIPT_FILENAME']) != 'login.php') {
        header("location:login.php");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recuento de gastos</title>
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <link href='css/pedidos.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/signin.css">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/pedidos.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <?php
            if(basename($_SERVER['SCRIPT_FILENAME']) == 'login.php') { ?>
                <li class="nav-item">
                  <a class="nav-link" href="login.php">Acceder</a>
                </li>
            <?php
            }
            else { ?>
                <li class="nav-item">
                  <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="logout.php">Salir</a>
                </li>
            <?php
            }
            ?>
          </ul>
        </div>
    </nav>
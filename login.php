<?php
    include('header.php');
?>
    <!-- Page Content -->
    <div class="container">

        <form class="form-signin" action="manage_login.php" method="post">
            <input name ='UserName' type="text" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
            <input name ='Password' type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
            <input class="btn btn-lg btn-primary btn-block submit" type="submit">
        </form>

    </div> <!-- /container -->

  </body>

</html>

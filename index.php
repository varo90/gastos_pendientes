<?php
    include('header.php');
    $current_date = date("t/m/Y",strtotime('-1 month'));
?>
    <!doctype html>
<html lang="es">
    <div class="container">
        Gastos pendientes hasta:
        <div class="input-group pedido">
            <input type="text" class="form-control datepicker_until" id="datepicker" value="<?php echo $current_date ?>">
            <span class="input-group-btn">
                <button id="get_file" class="btn btn-primary" type="button">Generar CSV</button>
            </span>
        </div>
    </div>
    <div id="ddd"></div>
</body>
</html>
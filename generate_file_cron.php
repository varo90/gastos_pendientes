<?php    
    
    date_default_timezone_set('Europe/Madrid');
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('functions.php');
    
    $date_until = date('Y-m-d');

    $cobros = get_cobros($date_until);
    $proveedores = get_proveedores($cobros);
    $rows = get_rows($proveedores,$cobros);
    
    $name = date_format(date_create_from_format('Y-m-d', $date_until), 'M_Y');
    $nom_fich = $name . '.csv';
    //$nom_fich = 'test.csv';
    $route_file = 'downloaded/'.$nom_fich;
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$nom_fich);
    $fp = fopen($route_file, 'w');

    $headers = array();
    $headers[] = 'Proveedor';
    $headers[] = 'Albaranes';
    $headers[] = 'BONOS PTE';
    $headers[] = 'Pedidos';
    fputcsv($fp, $headers,';');

    $print = array();
    foreach($rows as $cont => $row) {
        $print = array($row['Proveedor'],$row['albaran'],$row['factura'],$row['pedido']);
        fputcsv($fp, $print,';');
    }
    
    fclose($fp);

    send_email($route_file);